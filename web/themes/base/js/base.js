/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.scrolltoAnchor = {
    attach: function (context, settings) {
      $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
          scrollTop: $($.attr(this, 'href')).offset().top - 455
        }, 500);
      });
    }
  };


  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      $(".paragraph--type--call-to-action .field-name-field-title").click(function(){
        $(this).toggleClass("callout-open");
      });
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.slides = {
        attach: function (context, settings) {
            $(context).find('.field-name-field-slides.swiper-container').once('ifLatestSlider').each(function (){
                var slides = new Swiper ('.field-name-field-slides.swiper-container', {
                    autoplay: true,
                    loop: true,
                    effect: 'fade',
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullets',
                        clickable: true,
                    },
                });
            });
        }
    };

    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.offCanvas = {
        attach: function (context, settings) {
            $(context).find('#offCanvasRightOverlap').once('ifRightCanvas').each(function () {
                $menuButton = $('#menuButton');
                $(this).on("opened.zf.offcanvas", function(e){
                    $menuButton.addClass('is-active');
                });
                $(this).on("closed.zf.offcanvas", function(e){
                    $menuButton.removeClass('is-active');
                });
            });
        }
    };

  Drupal.behaviors.fixedmenu = {
    attach: function (context, settings){
      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        var objectSelect = $(".view-mode-full");
        var objectPosition = objectSelect.offset().top;
        if( scroll > objectPosition ) {
          $('body').addClass('fixed');
        } else {
          $('body').removeClass('fixed');
        }
      });
    }
  };

})(jQuery, Drupal);
